﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickUpWeapon : MonoBehaviour
{
    public GameObject pickupBtn;
    public GameObject crossHair;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "pistolDrop")
        {
            pickupBtn.gameObject.SetActive(true);

        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.name == "pistolDrop")
        {
            pickupBtn.gameObject.SetActive(false);

        }
    }
}
