﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class weaponPickUp : MonoBehaviour
{

    public Button pickUpBtn;
    public GameObject Pistol;
    // Start is called before the first frame update
    void Start()
    {
        pickUpBtn.onClick.AddListener(PistolTrue);
    }

    // Update is called once per frame
    void PistolTrue()
    {
        Pistol.gameObject.SetActive(true);
    }
}
