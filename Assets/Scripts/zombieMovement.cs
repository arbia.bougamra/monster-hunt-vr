﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombieMovement : MonoBehaviour
{
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {

    transform.Translate(Vector3.forward * Time.deltaTime * 0.3f);
    //rb.AddForce(transform.up * 0.5f * Time.deltaTime);
    transform.LookAt(Camera.main.transform.position);
    transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
    }
}
