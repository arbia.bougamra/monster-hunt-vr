﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionwithCamera : MonoBehaviour
{
    bool zombieIsThere;
    float timer;
    int atktime;
    private GameControllerScript gameController;

    // Start is called before the first frame update 
    void Start()
    {
        atktime = 2;
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        if(gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent <GameControllerScript> ();
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(zombieIsThere && timer >= atktime)
        {
            Attack();
        }

        if(!zombieIsThere)
        {
            Walk();
        }
        
    }

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.tag=="MainCamera")
            {
                 zombieIsThere = true;
            }
    
    }

    void OnCollisionExit(Collision col) {
        if (col.gameObject.tag=="MainCamera")
            {
                 zombieIsThere = false;
            }
    
    }

    void Attack(){

        timer = 0f;
        GetComponent<Animator> ().Play ("attack");
        gameController.zombieAttack (zombieIsThere);
    }
    

    void Walk(){
        
        GetComponent<Animator> ().Play ("walk");
    }
}
