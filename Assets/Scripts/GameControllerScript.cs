﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControllerScript : MonoBehaviour
{
    public GameObject bloodyScreen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void zombieAttack(bool zombieIsThere){
        bloodyScreen.gameObject.SetActive (true);
        StartCoroutine (wait2seconds ());
    }
    

    IEnumerator wait2seconds()
    {
    yield return new WaitForSeconds (2f);
    bloodyScreen.gameObject.SetActive (false);

    }

}
