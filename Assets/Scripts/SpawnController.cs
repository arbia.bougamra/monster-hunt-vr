﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnController : MonoBehaviour{

    public GameObject zombie;
    public Button startBtn;

    // Start is called before the first frame update
    void Start()
    {
        startBtn.onClick.AddListener (startInvoke);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void startInvoke()
    {
            InvokeRepeating("spawn", 0f, 30f);
    }

    void spawn()
    {
        Vector3 position = new Vector3 (Random.Range(0, -8f), Random.Range(0, 0f), Random.Range(0, 9f));        
        Instantiate (zombie, position, Quaternion.Euler(0,0,0));
    }
}
