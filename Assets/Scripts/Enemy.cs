﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health = 30f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(float damage)
    {
        health -= damage; //decrease health
        if (health <= 0f)
        {
            Die();//die
        }
    }

    private void Die()
    {
        Destroy(gameObject, 1f);
    }
}
