﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class reload : MonoBehaviour
    

{

    public Button reloadBtn;
    // Start is called before the first frame update
    void Start()
    {
        reloadBtn.onClick.AddListener(ReloadAnimation);
    }


    void ReloadAnimation()
    {
        gameObject.GetComponent<Animator>().SetTrigger("reload");
    }
}